﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE rfc SYSTEM "http://xml.resource.org/authoring/rfc2629.dtd"
[
<!ENTITY RFC1035 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.1035.xml'>
<!ENTITY RFC2045 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.2045.xml'>
<!ENTITY RFC2119 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml'>
<!ENTITY RFC3492 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.3492.xml'>
<!ENTITY RFC4033 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.4033.xml'>
<!ENTITY RFC4343 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.4343.xml'>
<!ENTITY RFC5890 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.5890.xml'>
<!ENTITY RFC6376 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.6376.xml'>
<!ENTITY RFC7033 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.7033.xml'>
<!ENTITY RFC7489 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.7489.xml'>
]>
<?rfc toc="yes"?>
<rfc category="info" docName="draft-sanz-openid-dns-discovery-01" ipr="trust200902">
    <front>
        <title abbrev="OpenID DNS Discovery">OpenID Connect DNS-based Discovery</title>
        <author fullname="Vittorio Bertola" initials="V." surname="Bertola">
            <organization abbrev="Open-Xchange">Open-Xchange</organization>
            <address>
                <postal>
                    <street>Via Treviso 12</street>
                    <code>10144</code>
                    <city>Torino</city>
                    <country>Italy</country>
                </postal>
                <email>vittorio.bertola@open-xchange.com</email>
                <uri>https://www.open-xchange.com</uri>
            </address>
        </author>
        <author fullname="Marcos Sanz" initials="M." surname="Sanz">
            <organization abbrev="DENIC eG">DENIC eG</organization>
            <address>
                <postal>
                    <street>Kaiserstraße 75 - 77</street>
                    <code>60329</code>
                    <city>Frankfurt am Main</city>
                    <country>Germany</country>
                </postal>
                <email>sanz@denic.de</email>
                <uri>https://www.denic.de</uri>
            </address>
        </author>
        <date/>
        <abstract>
<t>
The following document describes a DNS-based mechanism for a client to discover an OpenID Identity Provider given an Identifier of the End-User, as a complementary alternative to the existing WebFinger-based mechanism.
</t>
        </abstract>
    </front>
    <middle>
<section title="Introduction">
<t>
"OpenID Connect Discovery 1.0" <xref target="OpenID.Discovery"></xref> uses WebFinger <xref target="RFC7033"></xref> to locate the OpenID Provider for an End-User in a process called “issuer discovery”. While this mechanism has been in place for quite some time now and it has proven to work, it presents some operational inconveniences: A (dedicated) WebFinger service has to be setup and operated on top of an HTTPS server. Furthermore: in an OpenID deployment with distributed resources, each resource would have to be running its own WebFinger service to point to its issuer. This presents scaling/operating challenges, especially in a scenario where the deployment happens at Internet scale and each resource may use a different, personal domain name.
</t><t>
This document presents a lightweight discovery mechanism based on top of DNS (and DNS has to be setup anyway for the WebFinger approach to work). This so-called “DNS-based discovery” does not disrupt the existing discovery specification and is presented as an alternative discovery process, compatible to the existing issuer discovery process described in "OpenID Connect Discovery 1.0" <xref target="OpenID.Discovery"></xref> if interpreted as a so-called “out-of-band” mechanism.
</t><t>
Additionally the mechanisms described in this document enrich the discovery process by allowing to discover not only the issuer, but also potential Claims Providers <xref target="OpenID.Core"></xref> available for a given End-User identifier.
</t>
</section>
        <section title="Requirements Notation and Conventions">
            <t>
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in <xref target="RFC2119"></xref>.
</t><t>
Throughout this document, values are quoted to indicate that they are to be taken literally. When using these values in protocol messages, the quotes MUST NOT be used as part of the value.
            </t>            
        </section>
        <section title="DNS-based Discovery Resource Record">
            <t>
The format defined here has been inspired by the DMARC <xref target="RFC7489"></xref> and DKIM <xref target="RFC6376"></xref> specifications and some text passages have been directly copied from those standards to allow for a reading without following references.
</t><t>
DNS-based discovery metadata are stored as DNS TXT records in subdomains named "_openid". The underscore construct is used to define a semantic scope for DNS records that are associated with the parent domain (s.  <xref target="I-D.ietf-dnsop-attrleaf">"DNS Scoped Data Through Global '_Underscore' Naming of Attribute Leaves"</xref>).
</t><t>
For example, the domain owner of "myname.example.com" would post OpenID configuration in a TXT record at "_openid.myname.example.com". This DNS-located OpenID metadata will hereafter be called the "OpenID record".
</t><t>
To allow for the use of e-mail addresses as resources, the following additional rules are defined:
<list style="symbols">
    <t>In case the resource identifier contains at least one "@" character, the rightmost "@" character is replaced by the string "._openidemail.", which introduces an additional DNS subdomain inside the identifier's domain name.</t>
    <t>If any character in the resulting resource identifier is disallowed from use in domain names as per <xref target="RFC1035"></xref> section 2.3.1, the punycoding algorithm defined in <xref target="RFC3492"></xref> is applied.</t>
</list>
</t><t>
Per <xref target="RFC1035"></xref> a TXT record can comprise several "character-string" objects. Where this is the case, the evaluator of the OpenID record must concatenate these strings by joining together the objects in order and parsing the result as a single string.
</t><t>
Content of OpenID records follow the extensible "tag=value" syntax for DNS-based key records defined in <xref target="RFC6376"></xref> and definition there applies. Specifically:
</t><t>

<list style="symbols">
<t>Values are a series of strings containing either plain text or "base64" text (as defined in <xref target="RFC2045"></xref>, Section 6.8). The definition of the tag will determine the encoding of each value.
</t><t>
Unencoded semicolon (";") characters must not occur in the value, since that separates tag-value pairs.
</t><t>
Whitespaces are allowed anywhere around tags. In particular, any whitespace after the "=" and any whitespace before a terminating ";" is not part of the value; however, whitespace inside the value is significant.
</t><t>
Tags must be interpreted in a case-sensitive manner. Values must be processed as case sensitive unless the specific tag description of semantics specifies case insensitivity. Host and domain names in this context are to be compared in a case insensitive manner, per <xref target="RFC4343"></xref>.
</t><t>
Tags with duplicate names must not occur within a single tag-list; if a tag name does occur more than once, the entire tag-list is invalid.
</t><t>
Tag=value pairs that represent the default value for optional records may be included to aid legibility.
</t><t>
Unrecognized tags must be ignored.
</t><t>
Tags that have an empty value are not the same as omitted tags. An omitted tag is treated as having the default value; a tag with an empty value explicitly designates the empty string as the value.
</t>
</list>

</t><t>
Only tags defined in this document or in later extensions are to be processed; note that given the rules of the previous paragraph, addition of a new tag into the registered list of tags does not itself require a new version of OpenID record to be generated (with a corresponding change to the "v" tag's value, see later), but a change to any existing tags does require a new version.
</t><t>
The following tags are introduced as the initial valid OpenID tags:
</t><t>

<list style="symbols">
<t>v: Version (plain-text; REQUIRED).  Identifies the record retrieved as a OpenID record. It must have the value of "OID1". The value of this tag must match precisely; if it does not or it is absent, the entire record must be ignored. It must be the first tag in the list.
</t><t>
iss: The designated hostname for the Issuer (plain-text; REQUIRED). Internationalized domain names must be encoded as A-labels, as described in Section 2.3 of <xref target="RFC5890"></xref>. That hostname can be used as the issuer location of an OpenID Connect Discovery 1.0 (Section 4) Configuration Request to discover its relevant OpenID endpoints. The issuer MAY contain port and/or path components (e.g. "issuer.example.com:8080/path").
</t><t>
clp: The designated hostname for the Claims Provider (plain-text; OPTIONAL). Internationalized domain names must be encoded as A-labels, as described in Section 2.3 of <xref target="RFC5890"></xref>. That hostname can be used by Identity Providers as a claim source for aggregated or distributed claims. Support for Aggregated Claims and Distributed Claims is OPTIONAL in the OpenID Core specification, so is the usage of this tag. The value of this tag MAY contain port and/or path components (e.g. "provider.example.com:8080/path").
</t></list>

</t><t>

<figure align="center" title="Example OpenID record" anchor="example_record">
                <preamble>The following is an example of a valid OpenID record for the domain example.com according to this specification:</preamble>
                <artwork xml:space="preserve" align="center">_openid IN TXT “v=OID1;iss=auth.freedom-id.de;clp=identityagent.de”</artwork>
</figure>
         
</t></section>

<section title="DNS-based Issuer Discovery Process">
<t>
DNS-based OpenID Provider Issuer discovery is the process of determining the location of the OpenID Provider with this specification.
</t><t>
DNS-based issuer discovery requires the following information to make a discovery request: 
</t><t>

<list style="symbols"><t>
resource - Identifier for the target End-User that is the subject of the discovery request
</t><t>
host - Hostname target of the discovery DNS query
</t></list>

</t><t>
To start discovery of OpenID endpoints, the End-User supplies an Identifier to the Relying Party. The RP applies the same normalization rules to the Identifier as described in "OpenID Connect Discovery 1.0" <xref target="OpenID.Discovery"></xref> to determine the Resource and Host. It is worth mentioning that as result of those rules the resource could exactly match the host (e.g. resource is “example.com” and host is “example.com”).
</t><t>
The RP will then follow the following lookup scheme:
</t><t>

<list style="numbers"><t>
The RP queries the DNS for an OpenID record at host. A possibly empty set of records is returned.
</t><t>
Records that do not start with a "v=" tag that identifies the current version of this document, or an older version of this document supported by the RP, are discarded. If a tag identifying the current version is found, all records identifying other versions are discarded. If no tag identifying the current version is found, but other tags identifying older versions are, the records using the latest supported version are kept, and all others are discarded.
</t><t>
If the remaining set contains multiple records or no records, the DNS-based discovery process terminates.
</t><t>
If the retrieved OpenID record does not contain a valid iss tag, the process terminates.
</t><t>
Once the Issuer has been extracted from the iss tag, the regular dynamic discovery process can be resumed in Section 4 “Obtaining OpenID Provider Configuration Information” of <xref target="OpenID.Discovery"></xref>.
</t></list>
</t><t>
As already mentioned in <xref target="OpenID.Discovery"></xref> it is also worth noting that no relationship can be assumed between the user input Identifier string and the resulting Issuer location.
</t>
</section>

<section title="Security Considerations">
<t>DNS-based discovery depends directly on the security of the DNS. OpenID records must be ignored if not deployed in parallel with DNSSEC <xref target="RFC4033"></xref>. DNSSEC validation of OpenID records MUST be performed at all time before using them for any purpose. DNSSEC-validation errors must result in abortion of this DNS-based discovery process.
</t>
</section>
<section title="IANA Considerations">
<t>tbd</t>
</section>
    </middle>
    <back>
        <references title="Normative References">
            &RFC1035;            
            &RFC2045;
            &RFC2119;
            &RFC3492;
            &RFC4033;
            &RFC4343;
            &RFC5890;
        </references>
        <references title="Informative References">
            &RFC6376;
            &RFC7033;
            &RFC7489;

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml3/reference.I-D.draft-ietf-dnsop-attrleaf-07.xml" ?>

      <reference anchor="OpenID.Core" target="http://openid.net/specs/openid-connect-core-1_0.html">
        <front>
          <title>OpenID Connect Core 1.0</title>

          <author fullname="Nat Sakimura" initials="N." surname="Sakimura">
            <organization abbrev="NRI">Nomura Research Institute, Ltd.</organization>
          </author>

          <author fullname="John Bradley" initials="J." surname="Bradley">
            <organization abbrev="Ping Identity">Ping Identity</organization>
          </author>

          <author fullname="Michael B. Jones" initials="M.B." surname="Jones">
            <organization abbrev="Microsoft">Microsoft</organization>
          </author>

          <author fullname="Breno de Medeiros" initials="B." surname="de Medeiros">
            <organization abbrev="Google">Google</organization>
          </author>

	  <author fullname="Chuck Mortimore" initials="C." surname="Mortimore">
	    <organization abbrev="Salesforce">Salesforce</organization>
	  </author>

          <date day="8" month="November" year="2014"/>
        </front>
      </reference>

      <reference anchor="OpenID.Discovery" target="http://openid.net/specs/openid-connect-discovery-1_0.html">
        <front>
          <title>OpenID Connect Discovery 1.0</title>

          <author fullname="Nat Sakimura" initials="N." surname="Sakimura">
            <organization abbrev="NRI">Nomura Research Institute,
            Ltd.</organization>
          </author>

          <author fullname="John Bradley" initials="J." surname="Bradley">
            <organization abbrev="Ping Identity">Ping Identity</organization>
          </author>

          <author fullname="Michael B. Jones" initials="M.B." surname="Jones">
            <organization abbrev="Microsoft">Microsoft</organization>
          </author>

          <author fullname="Edmund Jay" initials="E." surname="Jay">
            <organization>Illumila</organization>
          </author>

          <date day="8" month="November" year="2014"/>
        </front>

      </reference>
        </references>
    </back>
</rfc>
